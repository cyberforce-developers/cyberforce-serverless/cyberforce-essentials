export interface IBaseRequest {
  lifeCycle: string // possibly RequestLifeCycles
  status: number | null
  statusText: string | null
  message?: string
  data: any
  headers: any
}
