// ISO 639-1

enum Languages {
  english = 'en-GB',
  polish = 'pl-PL'
}

export default Languages
