import { IKeyAny } from '../../../common/intf/IKeyAny'

// Example interface for a converter function
export interface IPrismicComponentParser {
  component: string
  convert: PrismicComponentConverterFunction
}

// Converter function should return your custom interface of `IPrismicComponentBody` and map Prismic fields over to what you like
// It returns `null` if component isn't recognised
export type PrismicComponentConverterFunction = (label: string, prismicElement: any) => IKeyAny | null
