import { IKeyAny } from '../../../common/intf/IKeyAny'

// All slices of a layout after transformation by parser
export interface IPrismicSlices {
  [slice: string]: IPrismicSlice
}

// A single slice contains static components (non-repeatable) and repeatable as per Prismic design
export interface IPrismicSlice {
  static: IPrismicComponent[]
  repeatable: IPrismicComponent[][]
}

// Defines prismic content type (no matter if repetable or not), unified to something human-redeable (after parsing)
export interface IPrismicContentType {
  id: string
  type: string
  layout: boolean
  last_publication: string
  lang: string
  slices: IPrismicSlices
  components: IPrismicComponent[]
}

// A generic Prismic component definition after parsing, in a format that can be then processed i.e. by React or front-end
export interface IPrismicComponent {
  label: string
  component: string | null
  slice: string | null
  firstOfKind: boolean
  lastOfKind: boolean
  body: IKeyAny | IPrismicComponent[] | null
}
