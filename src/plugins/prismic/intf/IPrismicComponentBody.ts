import { IPrismicComponent } from './IPrismicComponent'
import { IPrismicSpan } from './IPrismicSpan'

/*
  During parsing, every raw Prismic component is transfered over to IPrismicComponent entity. One of its properties
  (check IPrismicComponent interface) is `body` that is specific for a component after transfer. It is being set by
  so called: "converter functions" where developer can use his own, or my (default) to determine which component
  (in Prismic) represents which components in the application.
*/

// Body for PrismicComponents.richComponent
export type IPrismicRichComponentBody = IPrismicComponent[]

// Body for PrismicComponents.textComponent
export interface IPrismicTextComponentBody {
  text: string
}

// Body for PrismicComponents.imageComponent
export interface IPrismicImageComponentBody {
  src: string
  alt: string
  dimensions: {
    width: number
    height: number
  }
  copyright: string | undefined
}

// Body for PrismicComponents.hyperlinkComponent
export interface IPrismicHyperlinkComponentBody {
  type: string
  href: string
  target: string | undefined
}

// Body for PrismicComponents.htmlTextComponent
export interface IPrismicHtmlTextComponentBody {
  tag: string
  text: string
  spans: IPrismicSpan[]
}
