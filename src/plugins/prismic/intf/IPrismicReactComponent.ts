import { IPrismicComponent } from './IPrismicComponent'

// Generic function if you don't care which component you render
export interface IPrismicComponentProps {
  component: IPrismicComponent | undefined
}

// For a specific component
export interface IPrismicBaseComponentProps {
  component: IPrismicComponent
}

export interface IPrismicHyperlinkComponentProps extends IPrismicBaseComponentProps {
  caption: string
}