Prismic

Get components from Repeatable by field and key
pages
 -> custom_comp_ref_name => pageurl
 -> component name
  getComps(repeatableContentType, refFieldName, refValue)
 getComp(repeatableContentType, refFieldName, refValue, compName)

Remove possibility of returning NULL components
Throw an error if it's wrong but don't return NULL

Remove possibility of returning NULL from body and return type any or array